//Tubería con nombre entre logger y tenis. Tenis escribe en la tubería y logger lo lee
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include "iostream"

#define NUM_MAX 100
int main(int argc, char *argv[])
{

	int fd=0;  //Abre el FIFO para lectura
	char cadena[NUM_MAX];

	//Crear FIFO

	if (mkfifo("./tmp/Tuberia", 0666) < 0){
	perror("Error al crear tuberia");
	return 0;
	}
	else printf("Tuberia creada.\n");

	//Abrir Tuberia
	fd = open("./tmp/Tuberia", O_RDONLY|O_CREAT|O_TRUNC);
	if (fd == -1) {
		perror("Error al abrir tuberia");
		return 1;
	}

	while (read(fd, cadena, sizeof(cadena))>0) {
			
		printf("%s \n", cadena);
	}
	close (fd);
	unlink("./tmp/Tuberia"); // Elimina el FIFO ya escrito.
	
	

	return 0;
}

