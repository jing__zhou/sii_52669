#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "DatosMemCompartida.h"

using namespace std; 

int main(void)
{
      
	char * p_org;
	DatosMemCompartida * p_mem;
	
        //Abrir el fichero de memoria compartida 
        int fd_mem = open("./tmp/mem.txt", O_RDWR);
        if (fd_mem < 0) {
           perror("Error apertura fichero de memoria compartida");
           exit(1);
        }
        
	//proyección en el fichero de memoria
	if ((p_org=(char *)mmap(NULL, sizeof (p_mem), PROT_WRITE | PROT_READ, MAP_SHARED, fd_mem, 0))==MAP_FAILED)
	{
		perror("Error en la proyeccion del fichero origen");
		close(fd_mem);
		exit(1);
	}
	
	// Se cierra el fichero
	close(fd_mem); 

	//asignar la direccion de comienzo de la region creada 
	
	p_mem=(DatosMemCompartida *)p_org;
	

	int respuesta;
	float centro_raqueta;

	//Seguimiento de la pelota
	while(1)
	{
 		centro_raqueta=(p_mem->raqueta_c.y1+p_mem->raqueta_c.y2)/2;
		if(centro_raqueta<p_mem->esfera_c.centro.y)
			p_mem->accion=1;
		else if (centro_raqueta>p_mem->esfera_c.centro.y)
			p_mem->accion=-1;
		else {p_mem->accion=0; usleep (2);}
			
	}		

	
}

