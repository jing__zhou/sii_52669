// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=5;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}
Esfera::Esfera(float rad, float x, float y, float vx, float vy)
{
    radio = rad;
    centro.x = x;
    centro.y = y;
    centro.x = vx;
    centro.y = vy;
}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*t;
}
void Esfera::CambiaRadio(float t)
{
	radio=radio-0.01;
	if (radio < 0.2)
	radio=1.5;
}
